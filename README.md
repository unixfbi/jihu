# jihu

## service-a 镜像仓库
```
registry.cn-beijing.aliyuncs.com/pgp/service-a:v0.1
```

## 操作命令记录
### Dockerfile文件内容
```
# Dockerfile
FROM golang:latest
MAINTAINER pangguoping
ENV GOPROXY=https://goproxy.cn
ADD service-a-main.tar.gz /opt/
RUN  cd /opt/service-a-main && go mod tidy && go build
WORKDIR /opt/service-a-main
CMD ["./service-a","--file","config.yaml"]


```
### 测试镜像
#### 构建镜像：
```
 docker build -t go-service-a:v4.1 .
```
#### 运行容器：
```
docker run --name=pgp-service-a-5 -itd go-service-a:v4.1
```
#### 查看容器状态：
```
$ docker ps 
CONTAINER ID   IMAGE               COMMAND                  CREATED          STATUS          PORTS     NAMES
c71cfe17ab9a   go-service-a:v4.1   "./service-a --file …"   4 seconds ago    Up 3 seconds              pgp-service-a-5
```

#### 进入容器：
```
docker exec -it pgp-service-a-5 /bin/bash
```

### push 镜像
```
docker login --username=ok815632410 registry.cn-beijing.aliyuncs.com
docker build -t registry.cn-beijing.aliyuncs.com/pgp/service-a:v0.1 .
docker push registry.cn-beijing.aliyuncs.com/pgp/service-a:v0.1
```
#### 测试：
```
docker run --name=pgp-service-a-5 -itd registry.cn-beijing.aliyuncs.com/pgp/service-a:v0.1
docker exec -it pgp-service-a-5 /bin/bash
```

### 使用helm部署镜像到k8s集群
```
helm repo add tkemarket https://market-tke.tencentcloudcr.com/chartrepo/opensource-stable

helm version

mkdir jihu  && cd jihu

helm create service-a

helm install --debug --dry-run   service-a  ./service-a --kubeconfig=/Users/panggp/.kube/config --kube-context=cls-69a1kl12-300000527957-context-default --namespace=service-pangguoping

helm install --debug    service-a  ./service-a --kubeconfig=/Users/panggp/.kube/config --kube-context=cls-69a1kl12-300000527957-context-default --namespace=service-pangguoping

helm list --kubeconfig=/Users/panggp/.kube/config --kube-context=cls-69a1kl12-300000527957-context-default --namespace=service-pangguoping

kubectl get pods -n service-pangguoping

kubectl get svc -n service-pangguoping

```

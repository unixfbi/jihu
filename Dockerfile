FROM golang:latest
MAINTAINER pangguoping
ENV GOPROXY=https://goproxy.cn
ADD service-a-main.tar.gz /opt/
RUN  cd /opt/service-a-main && go mod tidy && go build 
WORKDIR /opt/service-a-main
CMD ["./service-a","--file","config.yaml"]
